<?php
/**
 * Created by PhpStorm.
 * User: mrmoz
 * Date: 4/20/2016
 * Time: 3:32 PM
 */
include_once 'db_connect.php';
?>

<div id="background">
    <div id="form">
        <form role="form" action="../php/register_action.php" method="post">
            <div id="error-holder"></div>
            <div class="form-group">
                <label for="login">Login</label>
                <input type="text" class="form-control" id="login" name="login" placeholder="Your login">
            </div>
            <div class="form-group">
                <label for="pass">Password</label>
                <input type="password" class="form-control" id="pass" name="pass"
placeholder="Password">
            </div>
            <button type="button" id="proceed" class="btn btn-success">Sign Up</button>
            <button type="button" class="btn btn-default" onclick="signUp(0)">Cancel</button>
        </form>
    </div>
</div>

<script>
    
    $("#proceed").click(function(){
        var loginVar = $("#login").val();
        var passVar  = $("#pass").val();
        $.ajax({

        url: "../php/register_action.php",
        type: 'POST',
        data: {login: loginVar, pass: passVar},
        cache: false,
        success: function(html){
           $("#error-holder").html(html);
        }
    });
    });
    
    </script>

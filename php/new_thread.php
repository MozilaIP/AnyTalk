<?php
/**
 * Created by PhpStorm.
 * User: mrmoz
 * Date: 4/20/2016
 * Time: 3:32 PM
 */
include_once 'db_connect.php';
?>

<div id="background">
    <div id="form">
        <form role="form" action="../php/new_thread_action.php" method="post">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="Text">Text</label>
                <textarea type="password" class="form-control" id="text" name="text"
placeholder="Text"></textarea>
            </div>
            <button type="submit" class="btn btn-success"onclick="newThread('submit')">Create thread</button>
            <button type="button" class="btn btn-default" onclick="newThread('cancel')">Cancel</button>
        </form>
    </div>
</div>

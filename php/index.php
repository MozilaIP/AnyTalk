<?php

include_once "db_connect.php";

$sql = "SELECT * FROM users,threads WHERE users.`User_login` = threads.`User_ID` ORDER BY `threads`.`ID`;";
$posts = $conn->query($sql);

while ($row = $posts->fetch_assoc())
{ //echo "post";
    ?>
    <div class="row">
            <div class="col-md-12">
                <div class="topic-caption">
                    <ul>
                        <li class="bold question" id="Question" onclick="window.location.hash = 'topic?post_id=<?=$row['ID'];?>'"><?=$row['Title']?></li>
                    </ul>
                    <hr>
                    <div class="user_sign">
                        <div class="user_image" style="background-image: url(<?=$row['avatar'];?>)"></div>
                        <span class="user_name" onclick="showUserPage('<?=$row['User_ID'];?>')"><?=$row['User_ID'];?></span>
                    </div>
                </div>
                <div class="topic-stats">
                    <ul>
                        <? $count = $conn->query("SELECT COUNT(*) AS C FROM `posts` WHERE `Thread_ID`='".$row['ID']."'"); 
                            $count = $count->fetch_assoc();?>
                        <li class="bold replies-number"><?=$count['C'];?></li>
                        <li class="reply">replies</li>
                    </ul>
                </div>
            </div>
    </div>
<? }

?>
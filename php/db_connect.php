<?php
/**
 * Created by PhpStorm.
 * User: mrmoz
 * Date: 4/19/2016
 * Time: 6:37 PM
 */
$servername = "";
$username = "";
$password = "";
$db = "";
// Create connection
$conn = mysqli_connect($servername, $username, $password, $db);
$conn->query("SET NAMES utf8mb4");

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//echo "Connected successfully";

//noinspection JSUnresolvedFunction
var hash = window.location.hash;

function logOut() {
    $.ajax({
        url: "../php/logout.php",
        cache: false,
        success: function (html) {
            location.reload();
            checkLogin();
        }
    });
}

function checkLogin() {
    $.ajax({
        url: "../php/login_check.php",
        cache: false,
        success: function (html) {
            $('#login-signup').append(html);
        }
    });
}

function signUp(v) {

    if (v)
        $.ajax({
            url: "../php/register.php",
            cache: false,
            success: function (html) {
                //$('.content').empty();
                $('#body').append(html);
                //restoreOpacity();
                //window.location.hash = 'main';

            }
        });

    else
        $('#background').remove();

}

function newThread(v) {

    if (v == 'open')
        $.ajax({
            url: "../php/new_thread.php",
            cache: false,
            success: function (html) {
                $('#body').append(html);

            }
        });

    if (v == 'cancel')
        $('#background').remove();
}

function logIn(v) {
    if (v)
        $.ajax({
            url: "../php/login.php",
            cache: false,
            success: function (html) {
                $('#body').append(html);
            }
        });
    else
        $('#background').remove();
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function getPageByHash() {

    if (window.location.hash != hash) {
        hash = window.location.hash;
        if (hash.indexOf('#main') > -1)
            getIndexPage();

        else if (hash.indexOf('#topic') > -1)
            showTopic(getUrlVars()['post_id']);

        else if (hash.indexOf('#user') > -1)
            showUserPage(getUrlVars()['name']);

    }
}

setInterval(getPageByHash, 200);


function getIndexPage() {
    greyContent();

    $.ajax({
        url: "../php/index.php",
        cache: false,
        success: function (html) {
            $('.content').empty();
            $('.content').append(html);
            restoreOpacity();
            window.location.hash = 'main';

        }
    });
}

function showUserPage(name) {
    greyContent();
    $.ajax({

        url: "../php/profile.php",
        cache: false,
        data: {name: name},
        method: "GET",
        success: function (html) {
            $('.content').empty();
            $('.content').append(html);
            restoreOpacity();
            window.location.hash = 'user?name=' + name;
        }
    });
}

function showTopic(id) {
    // alert(id);
    greyContent();
    $.ajax({

        url: "../php/topic.php?post_id=" + id,
        cache: false,
        success: function (html) {
            $('.content').empty();
            $('.content').append(html);
            restoreOpacity();
            window.location.hash = 'topic?post_id=' + id;
        }
    });
}


function greyContent() {
    $('.content').css('opacity', '0.5');
}

function restoreOpacity() {
    $('.content').css('opacity', '1');
}

function emptyContent() {
    $('.content').empty();
}

function appendContent(html, callback) {
    //noinspection JSUnresolvedFunction
    $('.content').append(html);
    setTimeout(callback, 300);
}

function scrollup() {
    $("html, body").animate({scrollTop: 0}, "slow");
}
//unused:

$('#Question').click(function () {

    greyContent();

    $.ajax({
        url: "topic.html",
        cache: false,
        success: function (html) {
            emptyContent();
            appendContent(html, restoreOpacity());

        }
    });
});
